# Order Service

## Startup

This order service depends on MySql as its backing database

Use the followng command to create a mysql docker container
    
`docker run --name test -e MYSQL_ROOT_PASSWORD=secret -e MYSQL_DATABASE=orderdatabase -e MYSQL_USER=orderservice -e MYSQL_PASSWORD=password -d -p 3307:3306 mysql:8.0`


To connect to the mysql docker instance with the mysql client type in the following command

`docker exec -it  test mysql  -uroot -p`

After the mysql instance is launched successfully you can run the order service with the following command

 `./gradlew run`
 
You should then see the service starting up



##Extra Steps

This project uses gradle as its build system, To see a list of the available build commands for this project type

`./gradlew tasks`

**I would suggest watching this video before digging into micronaut stuff** 
[video](https://www.youtube.com/watch?time_continue=1&v=BL9SsY5orkA&feature=emb_logo)

For more information on gradle follow this [link](https://gradle.org/)

Micronaut Docs [here](https://docs.micronaut.io/latest/guide/index.html#introduction)

To install the micronaut cli follow this [link](https://docs.micronaut.io/latest/guide/index.html#buildCLI)

3 Hour video on how micronaut works [here](https://www.youtube.com/watch?v=S5yfTfPeue8)
