package com.dayebensa.order.controller;

import com.dayebensa.order.entities.*;
import com.dayebensa.order.services.OrderService;
import com.dayebensa.order.beans.SortingAndOrderArguments;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.*;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.UUID;


@Controller("/orders")
@Secured(SecurityRule.IS_AUTHENTICATED)
public class OrderController {

    private static final Logger LOG = LoggerFactory.getLogger(OrderController.class);

    protected final OrderService orderService;



    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @Get("/{?args*}")
    @Produces(MediaType.APPLICATION_JSON)
    public Iterable<Order> getAllOrders(@Valid SortingAndOrderArguments args) {
        return orderService.findAllOrders();
    }



    @Post
    public HttpResponse<Order> postOrder(@Body @Valid Order order) {


        Order postedOrder = orderService.saveOrder(order);
        return HttpResponse
                .created(postedOrder);
    }

    @Get("/{orderId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Optional<Order> getOrderById(String orderId){

        return orderService.findOrderById(orderId);
    }

    @Delete("/{orderId}")
    public HttpResponse<Order> deleteOrder(UUID orderId){
        orderService.deleteOrder(orderId);
        return HttpResponse.noContent();
    }

    @Put("/{orderId}")
    public HttpResponse<Order> updateOrder(UUID orderId, @Body @Valid Order order){

        orderService.updateOrder(order);
        return HttpResponse.noContent();
    }


    //Shipment  Endpoints

    @Post("/{orderId}/shipments")
    public HttpResponse<Shipment> createShipment(UUID orderId, @Body @Valid Shipment shipment){
        shipment.setOrderId(orderId);
        Shipment newShipment = orderService.saveShipment(shipment);
        return HttpResponse.created(newShipment);
    }

    @Put("/{orderId}/shipments/{shipmentId}")
    public HttpResponse<Shipment> updateShipment(UUID orderId, UUID shipmentId, Shipment shipment){
        orderService.updateShipment(shipment);
        return HttpResponse.noContent();
    }

    @Delete("/{orderId}/shipments/{shipmentId}")
    public HttpResponse<Shipment> deleteShipment(UUID orderId, UUID shipmentId){
        orderService.deleteShipment(shipmentId);
        return HttpResponse.noContent();
    }

    @Get("/{orderId}/shipments")
    public List<Shipment> getAllShipmentsByOrderId(UUID orderId){
        return orderService.findAllShipmentsByOrderId(orderId);
    }


    @Get("/{orderId}/shipments/{shipmentId}")
    public Optional<Shipment> getShipmentById(UUID orderId, UUID shipmentId){
        return orderService.findByShipmentId(shipmentId);
    }

    @Put("/{orderId}/shipments/{shipmentId}/products")
    public HttpResponse<Product> addProductToShipment(UUID orderId, UUID shipmentId, @Body @Valid Product product){
        product.setShipmentId(shipmentId);
        orderService.updateProduct(product);
        return HttpResponse.noContent();
    }

    @Get("/{orderId}/shipments/{shipmentId}/products")
    public List<Product> getProductsByShipmentId(UUID orderId, UUID shipmentId){
        return orderService.findProductByShipmentId(shipmentId);
    }

    @Delete("/{orderId}/shipments/{shipmentId}/products/{productId}")
    public HttpResponse<Product> deleteProductByShipmentId(UUID orderId, UUID shipmentId, UUID productId){
        orderService.deleteProductFromShipment(productId);
        return HttpResponse.noContent();
    }


    //Product Endpoints

    @Post("/{orderId}/products")
    public HttpResponse<Product> createProduct(UUID orderId, @Body @Valid Product product){
        product.setOrderId(orderId);
        return  HttpResponse.created(orderService.saveProduct(product));
    }

    @Get("/{orderId}/products")
    public List<Product> getAllProductsByOrderId(UUID orderId){
        return  orderService.findProductByOrderId(orderId);
    }

    @Put("/{orderId}/products/{productId}")
    public HttpResponse<Product> updateProduct(UUID orderId, UUID productId, @Body @Valid Product product){
        orderService.updateProduct(product);
        return  HttpResponse.noContent();
    }

    @Delete("/{orderId}/products/{productId}")
    public HttpResponse<Product> deleteProduct(UUID orderId,UUID productId){
        orderService.deleteProduct(productId);
        return  HttpResponse.noContent();
    }

    @Get("/{orderId}/products/{productId}")
    public Optional<Product> getProductById(UUID orderId,UUID productId){
        return  orderService.findProductById(productId);
    }



    //Product events
    @Post("/{orderId}/products/{productId}/events")
    public HttpResponse<ProductEvent> postProductEvent(UUID orderId, UUID productId, @Valid @Body ProductEvent productEvent){
        productEvent.setProductId(productId);
        return HttpResponse.created(orderService.saveProductEvent(productEvent));
    }

    @Get("/{orderId}/products/{productId}/events")
    public List<ProductEvent> getAllShipmentLogs(String orderId, UUID productId){
        return orderService.findProductEventByProductId(productId);
    }

    @Delete("/{orderId}/products/{productId}/events/{productEventId}")
    public HttpResponse<ProductEvent> deleteShipmentLog(UUID orderId, UUID productId, UUID productEventId){
        orderService.deleteProductEvent(productEventId);
        return HttpResponse.noContent();
    }




}
