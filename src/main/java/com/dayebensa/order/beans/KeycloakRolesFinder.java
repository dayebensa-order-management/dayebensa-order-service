package com.dayebensa.order.beans;

import io.micronaut.context.annotation.Replaces;
import io.micronaut.security.token.Claims;
import io.micronaut.security.token.DefaultRolesFinder;
import io.micronaut.security.token.RolesFinder;

import javax.annotation.Nonnull;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Custom implementation of {@link RolesFinder}. for Keycloak JWTs
 *
 * @author Paul Murage
 */

@Singleton
@Replaces(DefaultRolesFinder.class)
public class KeycloakRolesFinder implements RolesFinder {


    private static final String REALM_ACCESS_KEY = "realm_access";
    private static final String ROLES_KEY = "roles";

    @Nonnull
    @Override
    public List<String> findInClaims(@Nonnull Claims claims) {

        List<String> roles = new ArrayList<>();

        Object realmAccessObject = claims.get(REALM_ACCESS_KEY);


            if(realmAccessObject instanceof Map){
                Map<?,?> realmMap = (Map<?,?>)realmAccessObject;

                for(Object key : realmMap.keySet()){

                    if(key instanceof String && ROLES_KEY.equals((String)key)){
                        Object value = realmMap.get(key);

                        if (value instanceof List){
                            List<?> list = (List<?>)value;

                            for(Object role : list)
                                roles.add((String)role);
                        }
                    }
                }

            }


        return roles;
    }
}
