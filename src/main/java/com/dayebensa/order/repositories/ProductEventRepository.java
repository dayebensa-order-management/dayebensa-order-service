package com.dayebensa.order.repositories;

import com.dayebensa.order.entities.ProductEvent;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.CrudRepository;


import java.util.List;
import java.util.UUID;

@Repository
public interface ProductEventRepository extends CrudRepository<ProductEvent,UUID> {

    List<ProductEvent> findByProductId(UUID productId);

}
