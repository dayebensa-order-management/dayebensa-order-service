package com.dayebensa.order.repositories;

import com.dayebensa.order.entities.Order;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.CrudRepository;

import java.util.UUID;

@Repository
public interface OrderRepository extends CrudRepository<Order, UUID> {



}
