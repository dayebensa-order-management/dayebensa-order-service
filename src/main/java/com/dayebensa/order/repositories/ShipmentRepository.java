package com.dayebensa.order.repositories;

import com.dayebensa.order.entities.Shipment;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.CrudRepository;


import java.util.List;
import java.util.UUID;

@Repository
public interface ShipmentRepository extends CrudRepository<Shipment,UUID> {

    List<Shipment> findByOrderId(UUID orderId);

}
