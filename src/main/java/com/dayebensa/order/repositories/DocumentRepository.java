package com.dayebensa.order.repositories;

import com.dayebensa.order.entities.Document;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.CrudRepository;


import java.util.List;
import java.util.UUID;

@Repository
public interface DocumentRepository extends CrudRepository<Document,UUID> {

    List<Document> findByOrderId(UUID orderId);

}
