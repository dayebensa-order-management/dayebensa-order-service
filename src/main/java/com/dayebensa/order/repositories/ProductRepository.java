package com.dayebensa.order.repositories;

import com.dayebensa.order.entities.Product;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.PageableRepository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ProductRepository extends PageableRepository<Product, UUID> {

    List<Product> findByOrderId(UUID orderId);

    List<Product> findByShipmentId(UUID shipmentId);
}
