package com.dayebensa.order.services;

import com.dayebensa.order.entities.Order;
import com.dayebensa.order.entities.Product;
import com.dayebensa.order.entities.ProductEvent;
import com.dayebensa.order.entities.Shipment;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface OrderService {

    Order saveOrder(Order order);

    Optional<Order> findOrderById(String id);

    Iterable<Order> findAllOrders();


    void deleteOrder(UUID orderId);

    void updateOrder(Order order);

    Product saveProduct(Product product);

    void deleteProduct(UUID product);

    Product updateProduct(Product product);

    Optional<Product> findProductById(UUID id);

    List<Product> findProductByOrderId(UUID orderId);

    List<Product> findProductByShipmentId(UUID shipmentId);


    Shipment saveShipment( Shipment shipment);

    Optional<Shipment> findByShipmentId(UUID shipmentId);


    List<Shipment> findAllShipmentsByOrderId(UUID orderId);

    void deleteShipment(UUID shipmentId);

    void updateShipment(Shipment shipment);


    ProductEvent saveProductEvent(ProductEvent productEvent);

    List<ProductEvent> findProductEventByProductId(UUID productId);

    void deleteProductEvent(UUID productEventId);

    void deleteProductFromShipment(UUID productId);
}
