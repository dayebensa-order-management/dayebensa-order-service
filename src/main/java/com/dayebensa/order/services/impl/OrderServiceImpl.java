package com.dayebensa.order.services.impl;

import com.dayebensa.order.entities.Order;
import com.dayebensa.order.entities.Product;
import com.dayebensa.order.entities.ProductEvent;
import com.dayebensa.order.entities.Shipment;
import com.dayebensa.order.repositories.OrderRepository;
import com.dayebensa.order.repositories.ProductEventRepository;
import com.dayebensa.order.repositories.ProductRepository;
import com.dayebensa.order.repositories.ShipmentRepository;
import com.dayebensa.order.services.OrderService;


import javax.inject.Singleton;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Singleton
public class OrderServiceImpl implements OrderService {


    protected final OrderRepository orderRepository;

    protected final ProductRepository productRepository;

    protected  final ShipmentRepository shipmentRepository;

    protected  final ProductEventRepository productEventRepository;

    public OrderServiceImpl(OrderRepository orderRepository, ProductRepository productRepository,
                            ShipmentRepository shipmentRepository, ProductEventRepository productEventRepository) {
        this.productRepository = productRepository;
        this.orderRepository = orderRepository;
        this.shipmentRepository = shipmentRepository;
        this.productEventRepository = productEventRepository;
    }

    @Override
    public Order saveOrder(Order order) {
       return orderRepository.save(order);
    }

    @Override
    public Optional<Order> findOrderById(String uuid) {
        return orderRepository.findById(UUID.fromString(uuid));
    }

    @Override
    public Iterable<Order> findAllOrders() {
        return orderRepository.findAll();
    }


    @Override
    public void deleteOrder(UUID orderId) {

        orderRepository.deleteById(orderId);
    }

    @Override
    public void updateOrder(Order order) {
        orderRepository.update(order);
    }

    @Override
    public Product saveProduct(Product product) {
        return productRepository.save(product);
    }

    @Override
    public void deleteProduct(UUID id) {
        productRepository.deleteById(id);
    }

    @Override
    public Product updateProduct(Product product) {
         return productRepository.update(product);
    }

    @Override
    public Optional<Product> findProductById(UUID id) {
        return productRepository.findById(id);
    }

    @Override
    public List<Product> findProductByOrderId(UUID orderId) {
        return productRepository.findByOrderId(orderId);
    }

    @Override
    public List<Product> findProductByShipmentId(UUID shipmentId) {
        return productRepository.findByShipmentId(shipmentId);
    }

    @Override
    public Shipment saveShipment(Shipment shipment) {
        return shipmentRepository.save(shipment);
    }

    @Override
    public Optional<Shipment> findByShipmentId(UUID shipmentId) {
        return shipmentRepository.findById(shipmentId);
    }


    @Override
    public List<Shipment> findAllShipmentsByOrderId(UUID orderId) {
        return shipmentRepository.findByOrderId(orderId);
    }

    @Override
    public void deleteShipment(UUID shipmentId) {
        shipmentRepository.deleteById(shipmentId);
    }

    @Override
    public void updateShipment(Shipment shipment) {
        shipmentRepository.update(shipment);
    }

    @Override
    public ProductEvent saveProductEvent(ProductEvent productEvent) {
        return productEventRepository.save(productEvent);
    }

    @Override
    public List<ProductEvent> findProductEventByProductId(UUID productId) {
        return productEventRepository.findByProductId(productId);
    }

    @Override
    public void deleteProductEvent(UUID productEventId) {
        productEventRepository.deleteById(productEventId);
    }

    @Override
    public void deleteProductFromShipment(UUID productId) {
        Optional<Product> product  = productRepository.findById(productId);
        product.ifPresent(prod -> {
            prod.setShipmentId(null);
            productRepository.update(prod);
        });
    }


}
