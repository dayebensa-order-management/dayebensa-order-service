package com.dayebensa.order.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import javax.persistence.AttributeConverter;
import java.util.Map;

public class HashMapConverter implements AttributeConverter<Map<String,Object>,String> {

    private ObjectMapper objectMapper = new ObjectMapper();
    private static final Logger LOG = LoggerFactory.getLogger(HashMapConverter.class);

    @Override
    public String convertToDatabaseColumn(Map<String, Object> attribute){
        String string = null;

        try {
            string = objectMapper.writeValueAsString(attribute);
        }catch(Exception e){
            LOG.error("Problem converting map to json string",e);
        }

        return string;
    }

    @Override
    public Map<String, Object> convertToEntityAttribute(String dbData) {

        Map<String,Object> map = null;

        try{
            map = objectMapper.readValue(dbData,Map.class);
        } catch (Exception e){
            LOG.error("Problem converting json string to map",e);
        }
        return map;
    }
}
