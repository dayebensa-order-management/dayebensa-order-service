package com.dayebensa.order.entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.time.LocalDate;

@Embeddable
public class LetterOfCredit {

    @Column(name = "opening_date")
    private LocalDate openingDate;

    @Column(name = "letter_of_credit_number", length = 50)
    private String number;

    @Column(name = "expiry_dated")
    private LocalDate expiryDate;

    public LocalDate getOpeningDate() {
        return openingDate;
    }

    public void setOpeningDate(LocalDate openingDate) {
        this.openingDate = openingDate;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public LocalDate getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(LocalDate expiryDate) {
        this.expiryDate = expiryDate;
    }
}
