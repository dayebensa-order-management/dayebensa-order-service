package com.dayebensa.order.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "shipment")
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class Shipment extends UpdatableEntity{

    public Shipment() {
    }


    @Column(length = 30)
    private String destination;

    @Column(name = "order_id", length = 16)
    private UUID orderId;


    @Column(name = "expected_shipment_date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate expectedShipmentDate;

    @Embedded
    private LetterOfCredit letterOfCredit;


    /*@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "order_id",insertable = false,updatable = false)
    @JsonManagedReference(value = "order-shipment")
    private Order order;*/

    /*@OneToMany(mappedBy = "shipment",cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    @JsonBackReference(value = "shipment-product")
    private List<Product> products;*/

}
