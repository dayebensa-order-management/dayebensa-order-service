package com.dayebensa.order.entities;


import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.UUID;


@Entity
@Table(name = "product_event")
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class ProductEvent  extends BaseEntity{

    public enum EventType {
        ORDER_CREATED_LOG("Order was created by dayebensa"),
        PROCESSING_LOG("Order has been sent out for processing"),
        PRESHIPMENT_SAMPLE_LOG("Preshipment Sample has been sent out"),
        REPROCESSING_LOG("Order is undergoing reprocessing"),
        GOVERNMENT_INSPECTION_LOG("Order is currently under Government Inspection"),
        TRANSIT_TO_PORT_LOG("Order is currently in transit to the port"),
        AT_PORT_LOG("Order is currently at port"), //TODO: find better wording
        SHIPPED_LOG("Order has shipped"),
        PRESHIPMENT_SAMPLE_REJECTION("Preshipment sample was rejected"),
        GOVERNMENT_INSPECTION_REJECTION("Order failed government inspection");

        private final String description;

        EventType(String description){
            this.description = description;
        }

        public String getDescription(){
            return description;
        }
    }
    public ProductEvent() {
    }


    @Column(name = "event_type")
    private EventType eventType;

    @Column(name = "product_id", length = 16)
    private UUID productId;

    @Column(length = 100)
    private String notes;


    @Column(length = 30)
    private String location;


   /* @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "product_id",insertable = false,updatable = false)
    @JsonManagedReference
    private Product product;*/

}
