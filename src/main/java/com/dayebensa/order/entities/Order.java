package com.dayebensa.order.entities;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "order_table")
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class Order extends UpdatableEntity{

    public enum OrderStatus {
        IN_TRANSIT,
        SHIPPED,
        NEW
    }


    public Order() {
    }


    @Column(name = "customer_name")
    private String customerName;

    @Column(name = "contract_number")
    private String contractNumber;

    @Column
    @Enumerated(EnumType.STRING)
    private OrderStatus status;


    @Column(length = 10)
    private String currency;

    @Column(length = 50, name = "financial_institution")
    private String financialInstitution;


    @Column(name = "bag_weight", columnDefinition = "DECIMAL(10,2)")
    private Double bagWeight;

    /*@OneToMany(mappedBy = "order",cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    @JsonBackReference(value = "order-shipment")
    private List<Shipment> shipments;*/

    /*@OneToMany(mappedBy = "order",cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    @JsonBackReference(value = "order-product")
    private List<Product> products;*/

    @JsonIgnore
    @OneToMany(mappedBy = "order",cascade = CascadeType.ALL)
    private List<Document> documents;

}
