package com.dayebensa.order.entities;


import javax.persistence.*;
import java.sql.Blob;
import java.util.UUID;

@Entity
@Table(name = "document")
public class Document {

    public enum DocumentType {
        PDF,
        EXCEL,
        TEXT,
        JPEG,
        PNG
    }
    public Document() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(length = 16)
    private UUID id;

    @Column(name = "order_id", length = 16)
    private UUID orderId;

    @Column(name = "document_type", length = 60)
    private String documentType;

    @Column
    private String name;

    @Lob
    @Column(columnDefinition = "LONGBLOB")
    private byte[] data;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "order_id",insertable = false,updatable = false)
    private Order order;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getOrderId() {
        return orderId;
    }

    public void setOrderId(UUID orderId) {
        this.orderId = orderId;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }
}
