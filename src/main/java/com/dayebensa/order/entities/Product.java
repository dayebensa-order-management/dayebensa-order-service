package com.dayebensa.order.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;


@Entity
@Table(name = "product")
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class Product extends UpdatableEntity{


    @Column(name = "order_id", length = 16)
    private UUID orderId;

    @Column(name = "shipment_id")
    private UUID shipmentId;

    @Column
    private Integer grade;

    @Column(name = "coffee_type", length = 30)
    private String coffeeType;

    @Column(length = 30)
    private String region;

    @Column(name = "processing_type", length = 30)
    private String processingType;

    @Column(name = "crop_year")
    private Integer cropYear;

    @Column(name = "number_of_bags")
    private Integer numberOfBags;



    @Column(name = "unit_price", columnDefinition = "DECIMAL(10,2)")
    private BigDecimal unitPrice;

    @Column(length = 30)
    private String certification;

    /*@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "order_id",insertable = false,updatable = false)
    @JsonManagedReference(value = "order-product")
    private Order order;*/

   /* @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "shipment_id",insertable = false,updatable = false)
    @JsonManagedReference(value = "shipment-product")
    private Shipment shipment;*/

    /*@OneToMany(mappedBy = "product",cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    @JsonBackReference
    private List<ProductEvent> productEvents;*/

}
