package com.dayebensa.order.services

import com.dayebensa.order.entities.Order
import com.dayebensa.order.repositories.OrderRepository
import io.micronaut.test.annotation.MicronautTest
import spock.lang.Specification
import spock.lang.Unroll

import javax.inject.Inject


@MicronautTest
class OrderServiceSpec extends Specification{

    @Inject
    OrderService orderService;

    Order order;
    UUID id;
    OrderRepository repository;

    void setup(){

        repository = Stub(OrderRepository)

        id = UUID.randomUUID();

        order = new Order()
        order.setId(id)
        order.setBagWeight(80)
        order.setContractNumber(12343235)
        order.setCustomerName("customer-id")
        order.setPrice(new BigInteger("70"))
        order.setStatus(Order.OrderStatus.IN_TRANSIT)
        order.setTotalQuantity(3200)
    }


    @Unroll
    void "Retrieve order  test"() {
        given: "An order"
        repository.findById(id) >> order

        when:
        def persistedOrder = orderService.findOrderById(id.toString())

        then: "Check for equality"
        !persistedOrder.isPresent()
        persistedOrder.get() == order

    }


}
