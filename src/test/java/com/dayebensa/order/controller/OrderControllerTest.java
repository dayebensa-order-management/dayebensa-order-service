package com.dayebensa.order.controller;

import com.dayebensa.order.repositories.OrderRepository;
import com.dayebensa.order.services.OrderService;
import com.dayebensa.order.services.impl.OrderServiceImpl;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.client.RxHttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import io.micronaut.test.annotation.MicronautTest;
import io.micronaut.test.annotation.MockBean;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import javax.inject.Inject;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

//TODO: fix
@MicronautTest
public class OrderControllerTest {

    @Inject
    @InjectMocks
    @Client("/")
    RxHttpClient client;

    @Inject
    private OrderService orderService;

    @Mock
    private OrderRepository orderRepository;

    @Before
    public void before (){
        MockitoAnnotations.initMocks(this);
    }


    public void testRoot(){
        HttpRequest<String> request = HttpRequest.GET("/order");

        String body = client.toBlocking().retrieve(request);

        assertNotNull(body);

        assertEquals("Order Created",body);
    }


//    @Test
    public void testNonExistentOrder(){

        when(orderService.findOrderById("99")).thenReturn(Optional.empty());

        HttpClientResponseException thrown = assertThrows(HttpClientResponseException.class, () -> {
            client.toBlocking().exchange(HttpRequest.GET("/orders/99"));
        });

        assertNotNull(thrown.getResponse());
        assertEquals(HttpStatus.NOT_FOUND, thrown.getStatus());
    }

    @MockBean(OrderServiceImpl.class)
    public OrderService orderService(){
        return Mockito.mock(OrderService.class);
    }
}
