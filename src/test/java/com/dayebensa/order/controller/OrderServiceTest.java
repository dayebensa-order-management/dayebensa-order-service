package com.dayebensa.order.controller;

import com.dayebensa.order.entities.Order;
import com.dayebensa.order.repositories.OrderRepository;

import com.dayebensa.order.services.OrderService;
import io.micronaut.test.annotation.MicronautTest;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import javax.inject.Inject;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertTrue;


//TODO: Fix
@MicronautTest
public class OrderServiceTest {


    @InjectMocks
    private OrderService orderService;

    @Mock
    private OrderRepository orderRepository;

    private Order order;
    @Before
    public void before (){
        MockitoAnnotations.initMocks(this);
        order = new Order();
        order.setId(UUID.fromString("d532cc4d-3546-4e14-b4e7-bc1cf06c4453"));

    }



    public void findOrderTest() {
        Mockito.when(orderRepository.findById(UUID.fromString("d532cc4d-3546-4e14-b4e7-bc1cf06c4453"))).thenReturn(Optional.of(order));


        Optional<Order> found = orderService.findOrderById("d532cc4d-3546-4e14-b4e7-bc1cf06c4453");

        assertTrue(found.isPresent());
    }
}
